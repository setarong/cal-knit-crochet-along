const passport = require('passport');
const LocalStratege = require('passport-local').Strategy;
const db = require('./db');

module.exports = () => {
    // serializeUser는 성공한 user를 전달받아 세션에 저장합니다.
    passport.serializeUser((user, done) => {    // strategy 성공 시 호출됨
        console.log("serialize user");
        done(null, user);  // 여기 user가 deserializeUser의 첫번째 매개변수로 이동
    });
    // deserializeUser는 서버로 들어오는 요청마다 세션 정보를 실제 DB 데이터와 비교합니다.
    passport.deserializeUser((user, done) => {  // 매개변수 user는 serializeUser의 done의 인자 user를 받은것.
        console.log("deserialize user!");
        done(null, user);   // 여기 user가 request.user가 됩니다.
    });

    passport.use(new LocalStratege({    // local 전략을 세운다
        usernameField: 'id',
        passwordField: 'pw',
        session: true,  // 세션 저장 여부
        passReqToCallback: false,
    }, (id, password, done) => {    // verify 하는부분
        db.query("SELECT * FROM users WHERE id IN ($1)", [id], (err, result) => {
            if (err) return done(err);
            if (result.rowCount == 0) return done(null, false, {message:'Id does not exist'});
            if (result.rows[0].password == password) {  // TODO 암호화
                return done(null, result.rows[0]);
            } else {
                return done(null, false, {message:'wrong password'});   // 이 message를 res로 보낼 수 없는건가?
            }
        });
    }));
}