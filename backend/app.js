var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var db = require('./db');

var indexRouter = require('./routes/index');
var loginRouter = require('./routes/login');
var apiRouter = require('./routes/api');
// var usersRouter = require('./routes/users');

// passport session관련
const session = require('express-session');
const FileStore = require('session-file-store')(session);
const flash = require('connect-flash');
const passport = require('passport');
const passportConfig = require('./passport');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade'); // 하나는 반드시 정의되어 있어야 한다.

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// 경로 바꿔줍니다. frontend > dist (vue build 결과물이 위치하는 장소로)
// app.use(express.static(path.resolve(__dirname, '../frontend/dist/')));

// DB setting
db.connect(function(err, client, done) {
  if (err) {
    console.log("db connect error : " + err);
  }
});
// passport session setting
app.use(session({
  secret:'비밀코드', 
  resave: true, 
  saveUninitialized: false,
  store: new FileStore({path : './sessions'})
}));  // 세션 활성화
// app.use(flash());
app.use(passport.initialize()); // passport 구동
app.use(passport.session()); // 세션 연결
passportConfig();

// router setting
app.use('/', indexRouter);
app.use('/login', loginRouter);
app.use('/api', apiRouter);
// app.use('/users', usersRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
