var express = require('express');
var router = express.Router();
var multiparty = require('multiparty');
var fs = require('fs');

router.post('/upload', (req, res) => {
    console.log('req' + req);
    var form = new multiparty.Form();
    var serverFilename;

    form.on('field', (name, value) => {
        originFilename = value;
        console.log('name : ' + name + ' / value : ' + value);
    });
    // file upload handling
    form.on('part', (part) => {
        console.log("part on!");
        let size;
        if (part.filename) {
            serverFilename = part.filename;
            size = part.byteCount;
        } else {
            serverFilename = part.name;
            part.resume();
        }
        var writeStream = fs.createWriteStream('./public/tmp/' + serverFilename);
        writeStream.filename = serverFilename;
        part.pipe(writeStream);

        part.on('data', (chunk) => {
            console.log(serverFilename + ' read ' + chunk.length + 'bytes');
        });
        part.on('end', () => {
            console.log(serverFilename + ' Part read complete');
            writeStream.end();
        });
    });

    form.on('close', () => {
        console.log("form close event");
        let funcNum = req.query.CKEditorFuncNum;
        // funcNum과 serverFileName 모두 맞아야 정상동작합니다..ㅠㅠ
        let data = "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction(" + funcNum + ",'/tmp/" + serverFilename + "','message')</script>";
        console.log(data);
        // res.contentType('json');
        // res.write(data);
        // res.end();
        res.send(data);
    });

    form.parse(req);
});

module.exports = router;