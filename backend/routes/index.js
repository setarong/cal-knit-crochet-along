var express = require('express');
var path = require('path');
var router = express.Router();

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

// var pool = new pg.Pool(config);

// router.get('/', function(req, res, next) {
//   // DB test
//   // pg 6.0버전 문법. 7.0부터는 config로 pg.Pool(config)생성한다.
//   // var connect = "tcp://postgres:1111@localhost:5432/postgres" // tcp://이용자:비밀번호@호스트:포트번호/데이터베이스이름
//   pool.connect(function(err, client, done) {
//     var query = client.query('select * from testdata;');
//     var rows = [];
//     query.then((result)=> {
//       console.log(result);
//       rows.push(result.rows);
//       res.send(rows);
//     }, (err) => {
//       console.log(err);
//     });
//     // pg 6.0버전 문법. 7.0부터 promise-then으로 바뀌었다.
//     // query.on('row', (row)=> {
//     //   rows.push(row);
//     //   res.send(rows);
//     // });
//     // query.on('end', (res)=> {
//     //   console.log(res);
//     // });
//     // query.on('error', (res)=> {
//     //   console.log(res);
//     // });
//     // done();
//   });
// });

router.get('/', function (req, res, next) {
  res.sendFile(path.join(__dirname, '../public', 'index.html'))
});

// logout
router.get('/logout', (req, res) => {
  req.logout();
  req.session.destroy((err) => {
    res.writeHead(200, {
      'Set-Cookie': [
        'nickname=; Max-Age=0',
        'isLogin=false; Max-Age=0'
      ]
    });
    res.end();
    // res.redirect('/');
  });
  // session 저장코드
  // req.session.save(()=> {
  //   res.redirect('/');
  // })
});

module.exports = router;
