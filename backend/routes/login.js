var express = require('express');
var router = express.Router();
const passport = require('passport');
var db = require('../db');

// test page
router.get('/', function(req, res, next) {
  console.log("server : login page");
});
// 로그인. passport custom. serialize, deserialize안탐
// router.post('/', (req, res) => {
//   passport.authenticate('local', (err, user, info) => {
//     if (user) {
//       res.writeHead(200, {
//         'Set-Cookie':[
//           'nickname=' + user.nickname
//         ],
//         // Location: '/'
//       });
//       let data = {'result' : 'ok'};
//       res.end(JSON.stringify(data));
//     } else {
//       res.send(info);
//     }
//   })(req, res); // closure. TODO 왜 클로저야 하는가?
// });
/* error를 서버에서 처리하는 경우 이렇게만 해도 됩니다 */
router.post('/', passport.authenticate('local'), (req, res) => {
  console.log('req.flash : ' + req.flash);
  console.log('req.user : ' + req.user);
  if (req.user) {
    res.writeHead(200, {
      'Set-Cookie' : ['nickname=' + req.user.nickname, 'isLogin=true']
    });
    let data = {'result' : 'ok'};
    res.end(JSON.stringify(data));
  } else {
    console.log('failed req', req);
  }
  
});
// 회원가입
router.post('/join', (req, res) => {
  form = req.body.form;
  db.query("INSERT INTO users VALUES($1, $2, $3, $4)", [form.id, form.nickname, form.email, form.password], (err, result) => {
    if (err) {
      res.send({"result" : "fail"});
    } else {
      res.send({"result" : "ok"});
    }
  });
});

module.exports = router;
