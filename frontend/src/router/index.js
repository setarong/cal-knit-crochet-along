import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/login/Login'
import SignUp from '@/components/login/SignUp'
import Cal from '@/components/cal/Cal'
import CreateCal from '@/components/cal/Create'

Vue.use(Router)

export default new Router({
  mode:'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path:'/login',
      name: 'login',
      component: Login
    },
    {
      path:'/signup',
      name:'SignUp',
      component: SignUp
    },
    {
      path: '/cal',
      name: 'Cal',
      component: Cal
    },
    {
      path: '/cal/create',
      component: CreateCal
    }
  ]
})
