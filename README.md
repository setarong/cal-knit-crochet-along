<!-- # CAL-knit,crochet along -->
# backend
## 기반기술
- express(서버구동)
- passport(로그인, 세션)

### 삽질 내역
- 외부에서 접근할 수 있는 부분은 /public 하위!
- multiparty 사용하여 파일 업로드시, form.parse(req, callback)에서 callback 구현할 경우 form.on 타지 않음. callback을 선언함이 곧 autoFile, autoUpload를 의미함. custom해주려면 form.parse(req) 만 작성하고 이벤트(part, close ...)등록할 것.
- ckeditor 이미지 업로드시 첫번째 파라미터(funcNum) 꼭 req.query.CKEditorFuncNum 넣어줘야 정상동작함.
```
<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction(" + funcNum + ",'/tmp/" + serverFilename + "','message')</script>>
```

# frontend
## 기반기술
- vue.js
- webpack,babel
- axios
- ckeditor (html editor)